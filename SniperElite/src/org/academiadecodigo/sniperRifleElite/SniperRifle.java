package org.academiadecodigo.sniperRifleElite;

import org.academiadecodigo.sniperRifleElite.objects.Destroyable;
import org.academiadecodigo.sniperRifleElite.objects.Enemy;

/**
 * Property:
 * bulletDamage; ---- int

 * Method():
 * shoot(Enemy); ---- void

 * Objective:
 * a)When shooting, it has a probability...
 * of hitting the target with a certain damage amount.
 */


public class SniperRifle {
    private int baseDamage = 10;

    public void shoot(Destroyable enemy) {
        int random = (int) (Math.random() * 3);
        enemy.hit(baseDamage * random);
    }

}

