package org.academiadecodigo.sniperRifleElite;

/** main(String[]) ---- void
creates Game Class

Objective:
a) When creating specify the number of game objects
b) Create a array of game objects with enemies and trees;
c) There is a higher probability of having enemies than trees;
d) Iterate through the array and shoot only the enemies;
e) Do NOT shoot the Trees!
f) Keep shooting the same enemy until it is dead;
g) At the end show the total number of shots fired.
* */

public class Main {

    public static void main(String[] args) {
        Game g = new Game(20);

        g.start(); // starts the game
    }
}
