package org.academiadecodigo.sniperRifleElite;

import org.academiadecodigo.sniperRifleElite.objects.*;

/**
 * Properties:
 * gameObjects; ---- GameObject[];
 * sniperRifle; ---- SniperRifle;
 * shotsFired; ----- int;

 * Methods():
 * start(); ------ void;
 * createObjects(); --- GameObject[];

 * Creates:
 * SniperRifle --- 1-1 (they get from one another)
 * ArmouredEnemy
 * SoldierEnemy
 * GameObject --- 1 (gets from Game)
 * Tree
 */

public class Game {

    public static final int GAME_OBJECTS = 10;

    //how many game objects - array
    private GameObject[] gameObjects;
    //how many sniperRifle
    private SniperRifle sniperRifle;
    //how many shots fired
    private int shotsFired;

    public Game(int gameObjects) {
        this.gameObjects = new GameObject[gameObjects];
    }

    public void start() {
        createObjects();
        sniperRifle = new SniperRifle();

        for (GameObject obj : gameObjects) {

            System.out.println(obj.getMessage());
            if (!(obj instanceof Destroyable)) {
                System.out.println("I love trees!");
                continue;
            }

            Destroyable enemy = (Destroyable) obj;

            while (!enemy.isDestroyed()) {
                sniperRifle.shoot(enemy);
                shotsFired++;
            }
        }

        System.out.println("Bang Bang! Fire! You've shot " + shotsFired + " bullets");
    }


    private void createObjects() {
        for (int i = 0; i < gameObjects.length; i++) {
            gameObjects[i] = newGameObject();
        }
    }

    private GameObject newGameObject() {

        int rand = (int) (Math.random() * 4);

        if (rand == 0) {
            return new Tree();
        }

        if (rand == 1) {
            return new Barrel();
        }

        return rand == 2 ? new SoldierEnemy() : new ArmouredEnemy();
    }

}

