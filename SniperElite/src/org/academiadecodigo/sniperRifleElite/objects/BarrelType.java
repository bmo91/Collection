package org.academiadecodigo.sniperRifleElite.objects;

/*Property:
max damage - int

*Method()
getMaxDamage() - int
 */

public enum BarrelType {
    PLASTIC(2),
    WOOD(4),
    METAL(1);

    private int maxDamage;

    BarrelType(int maxDamage) {
        this.maxDamage = maxDamage;
    }

    public static BarrelType getRandom() {
        return values()[(int) (Math.random() * values().length)];
    }

    public int getMaxDamage() {
        return maxDamage;
    }


}
