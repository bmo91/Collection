package org.academiadecodigo.sniperRifleElite.objects;
/**
 * Method():
 * getMessage(); ---- String
 */

public abstract class GameObject {

    public abstract String getMessage();
}
