package org.academiadecodigo.sniperRifleElite.objects;
/** Properties:
health; --- int
isDead; --- boolean

*Method():
isDead(); --- boolean
hit(int); --- void
getMessage(); -- String

*Extends GameObject && Implements Destroyable;
 */
public class Enemy extends GameObject implements Destroyable {

    private int health = 100;

    public void hit(int damage) {
        health -= damage;
    }

    public boolean isDestroyed() {
        return health <= 0;
    }

    public String getMessage() {
        if (isDestroyed()) {
            return "Bang bang! See you in hell!";
        }
        return "Fatherf*ck@r! Just die already!";
    }
}
