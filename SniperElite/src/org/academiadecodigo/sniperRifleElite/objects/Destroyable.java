package org.academiadecodigo.sniperRifleElite.objects;
/** Methods():
hit(int) -- void
isDestroyed() -- boolean
 */
public interface Destroyable {
    boolean isDestroyed();

    void hit(int damage);
}
