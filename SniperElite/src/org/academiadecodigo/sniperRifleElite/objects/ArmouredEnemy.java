package org.academiadecodigo.sniperRifleElite.objects;
/***vProperty:
armour; ---- int

*Method():
hit(int); ---- void

Extends Enemy
Objective:
a)if the armour has points it takes the damage,...
else the enemy loses health;
 */
public class ArmouredEnemy extends Enemy{

    private int armour = 20;

    @Override
    public void hit(int damage){
        armour -= damage;

         if(armour < 0) {
             super.hit(Math.abs(armour));
             armour = 0;
         }
    }
}