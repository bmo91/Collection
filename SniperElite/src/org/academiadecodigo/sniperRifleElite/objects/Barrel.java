package org.academiadecodigo.sniperRifleElite.objects;

/** Property:
barrelType ---- BarrelType
currentDamage --- int
destroyed ---- boolean

 *Method()
hit(int) --- void
isDestroyed() ---  boolean
getMessage() -- String
 */

public class Barrel extends GameObject {
    private BarrelType barrelType;
    private int currentDamage;
    private boolean destroyed;

    public Barrel() {
        barrelType = BarrelType.getRandom();
        currentDamage = barrelType.getMaxDamage();
    }

    public BarrelType getBarrelType() {
        return barrelType;
    }

    public void hit(int damage) {
        currentDamage -= damage;

        if (currentDamage <= 0) {
            destroyed = true;
        }
    }

    public boolean isDestroyed() {
        return destroyed;
    }

    public String getMessage() {
        return "Such a hero! That barrel sure got what was coming to it";
    }

}
