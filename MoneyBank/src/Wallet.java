public class Wallet {

    private int money = 0;

    public int getMoney() {
        return money;
    }

    public void add(int money) {
        this.money += money;
    }

    // returns the money removed from the wallet
    public int use(int money) {

        // exception case handling
        if (money > this.money) {
            int removedMoney = this.money;
            this.money = 0;
            return removedMoney;
        }

        // normal case
        this.money -= money;
        return money;

    }

    @Override
    public String toString() {
        return "Wallet{" +
                "money=" + money +
                '}';
    }
}
//teacher version ^


//My version v
//public class Wallet {
//    // balance >= 0
//    private int balance;
//    public Wallet(int balance){
//        if(balance >=0)
//            this.balance = balance;
//    }
//    //Float balance (é decimal)
//    private float balanceCount;
//    public Wallet(float balanceCount){
//        if(balanceCount >= 0.0)
//            this.balanceCount = balance;
//    }
//    //Balance>=0
//    public Wallet(){
//        balance = 0.0;
//    }
////    //Set Balance()
//    public float setBalance(){
//        balance = (float)(Math.random() * 1000;
//        balance = (float)(Math.round((balance*100.0)+.5)/100.0;
//        return balance;
//    }
//    //Deposit()
//    public float deposit (float amount){
//        balance = balance + amount;
//    }
//    //Draw()
//    public float draw(float amount){
//       balance = balance - amount;
//   }
//        //Get Balance
//    public float getBalance(){
//        balance = (float)(Math.round((balance*100.0)+.5)/100.0;
//        return balance;
//    }
//        //close
//    public float close(){
//        balance = 0.0;
//    }
//}
