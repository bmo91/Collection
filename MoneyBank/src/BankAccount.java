public class BankAccount {

    private int money;

    public int getMoney() {
        return money;
    }

    // returns the amount of money withdraw
    public int withdraw(int money) {

        // exception case handling
        if (money > this.money) {
            int removedMoney = this.money;
            this.money = 0;
            return removedMoney;
        }

        // normal case
        this.money -= money;
        return money;
    }

    public void deposit(int money) {
        this.money += money;
    }

    @Override
    public String toString() {
        return "PiggyBank{" +
                "money=" + money +
                '}';
    }

}
//teacher version ^


//My version v
//public class BankAccount {
//    // String "NIB"
//    private String name;
//    //Float balance
//    private float balance;
//
//    //initial Balance
//    public BankAccount(float initialBalance) {
//        balance = initialBalance;
//    }
//
//    public BankAccount(String name, float balance) {
//        this.name = name;
//        this.balance = balance;
//    }
//
//    //Balance>=0
//    public float BankAccount() {
//        balance = 0.0;
//    }
//
//    //Set Balance()
//    public float setBalance() {
//        balance = (float)(Math.random() * 1000);
//        balance = (float)(Math.round((balance * 100.0) + .5) / 100.0);
//        return balance;
//    }
//
//    //Deposit()
//    public void deposit(float amount) {
//        balance = balance + amount;
//    }
//
//    //Draw
//    public void draw(float amount) {
//        balance = balance - amount;
//    }
//
//    //Get Balance
//    public float getBalance() {
//        balance = (float)(Math.round((balance * 100.0) + .5) / 100.0);
//        return balance;
//    }
//
//    //close
//    public void close() {
//        balance = 0.0;
//    }
//
//    //transfer
//    public void transferTo(BankAccount bank, float x) {
//        if (x <= balance) {
//            draw(x);
//            bank.deposit(x);
//            System.out.println("\nTransfer successful. Transferred: $" + bank.getBalance());
//        } else if (x > balance) {
//            System.out.println("\nTransfer failed, not enough balance!");
//        }
//    }
//
//    //sets and gets name method
//    public void setName(String name) {
//        this.name = name;
//    }
//
//    public String getName() {
//        return name;
//    }
//}