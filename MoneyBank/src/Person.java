public class Person {

    private String name;

    private BankAccount piggyBank;
    private Wallet wallet;

    public Person(String name, BankAccount piggyBank, Wallet wallet, int money) {

        this.name = name;
        this.piggyBank = piggyBank;
        this.wallet = wallet;

        wallet.add(money);

    }

    // returns amount of money spent
    public int spend(int money)  {
        return wallet.use(money);
    }

    public void fillWallet(int money) {
        wallet.add(piggyBank.withdraw(money));
    }

    public void saveMoney(int money) {
        piggyBank.deposit(wallet.use(money));
    }

    @Override
    public String toString() {
        return "Person{" +
                "name=" + name +
                ", piggyBank=" + piggyBank +
                ", wallet=" + wallet +
                '}';
    }
}
//teacher version ^


//My version v
//public class Person {
//    //String name
//    private String name;
//    //1 wallet
//    public int wallet;
//    //1 account
//    public int bankAccount;
//    //Total Balance
//    public float balance;
//    public Person(String name){
//        this.name = name;
//        this.Wallet = wallet;
//        this.BankAccount = bankAccount;
//    }
//    public String getName;
//
//    //Total Money
//    public int balance() {
//        int money = wallet.balance + bankAccount.balance;
//        return money;
//    }
//    public String getName(){
//        return name;
//    }
//}