import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;

public class Testing {

    File image = new File("dina.jpg");
    File index = new File("index.html");
    File notFound = new File("notFound.html");
    byte[] resources;

    Socket browserSocket = null;
    Socket serverSocket = null;
    File file;

    protected void start() {

        ServerSocket s;
        System.out.println("WebServer starting on port ...");

        try {
            s = new ServerSocket(8000);

        } catch (IOException e) {
            System.out.println("Error: " + e);
            return;
        }

        System.out.println("Connecting...");


        try {
            Socket browserSocket = s.accept();

            System.out.println("Sending data...");
            BufferedReader in = new BufferedReader(new InputStreamReader(browserSocket.getInputStream()));
            BufferedOutputStream out = new BufferedOutputStream(browserSocket.getOutputStream());


            String line = "";

            while (true) {

                line = in.readLine();

                if (line.contains("GET")) {
                    System.out.println("GET was read...");
                    if (line.contains("index.html")) {
                        System.out.println("Index was read...");

                        String header = "HTTP/1.0 200 Document Follows\r\n" +
                                "Content-Type: text/html + image/jpg; charset=UTF-8\r\n" +
                                "Content-Length: " + index.length() + " \r\n" +
                                "\r\n";

                        FileInputStream reader = new FileInputStream("index.html");
                        resources = reader.readAllBytes();
                        out.write(header.getBytes());
                        out.write(resources);
                        reader.close();

                    } else if (line.contains("image.jpg")) {
                        System.out.println("Image was read...");

                        String imageHeader = "HTTP/1.0 200 Document Follows\r\n" +
                                "Content-Type: image\r\n" +
                                "Content-Length: " + image.length() + "\r\n" +
                                "\r\n";

                        FileInputStream reader = new FileInputStream("image.jpg");
                        resources = reader.readAllBytes();
                        out.write(imageHeader.getBytes());
                        out.write(resources);
                        reader.close();

                    } else if (line.contains("notFound.html")) {
                        System.out.println("404 Not Found");

                        String notFoundHeader = "HTTP/1.0 200 Document Follows\r\n" +
                                "Content-Type: text/html\r\n" +
                                "Content-Length: " + notFound.length() + "\r\n" +
                                "\r\n";

                        FileInputStream reader = new FileInputStream("notFound.html");
                        resources = reader.readAllBytes();
                        out.write(notFoundHeader.getBytes());
                        out.write(resources);
                        reader.close();
                    }

                }

                System.out.println(line);
                out.close();
                break;

            }
        } catch (IOException e) {

            System.err.println(e.getMessage());

        } finally {

            if (browserSocket != null) {

                System.out.println("Closing socket...");
                closeSocket(browserSocket);
                closeSocket(serverSocket);

            }

        }

    }

    private static void closeSocket(Closeable socket) {

        try {

            socket.close();

        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public static void main(String[] args) {
        Testing ts = new Testing();
        ts.start();
    }
}