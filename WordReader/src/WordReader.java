import java.io.*;
import java.util.Iterator;
import java.util.NoSuchElementException;
import java.util.Set;

public class WordReader implements Iterator<String> {

    private Set<String> removed;
    private BufferedReader reader;
    private String[] words;
    private int wordIndex;

    public WordReader(String filePath) {
        try {
            FileReader fileReader = new FileReader(filePath);
            reader = new BufferedReader(fileReader);
            readNextLine();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    private void readNextLine() {
        try {
            String line = reader.readLine();

            if (line == null) {
                words = null;
                return;
            }
            words = line.split(" ");
            wordIndex = 0;
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public boolean hasNext() {
        if(words == null){
            return false;
        } if (wordIndex == words.length){
            readNextLine();
            return hasNext();
        } return true;
    }

    @Override
    public String next(){
        if(!hasNext()){
            throw new NoSuchElementException();
        } wordIndex++;
        return words[wordIndex - 1];
    }

    @Override
    public void remove(){
        removed.add(words[wordIndex -1]);
    }
}

/*
public class WordReader implements Iterable<String>{
    private String file;
    private Set<String> remove;
    private BufferedRead words;
    private

    


}
 */