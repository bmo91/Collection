import java.io.*;
import java.io.IOException;

public class FileManager {
    public static void copy(String from, String to) throws FileNotFoundException {

        BufferedInputStream input = new BufferedInputStream(new FileInputStream(from));
        OutputStream output = new BufferedOutputStream(new FileOutputStream(to));

        try {
            int b = input.read();

            while (b > -1) {
                output.write(b);
                b = input.read();
            }

        } catch (IOException e) {
            System.err.println("Error copying file: " + e.getMessage());
            System.exit(1);
        } finally {
            closeStream(input);
            closeStream(output);
        }
    }

    private static void closeStream(Closeable stream) {
        try {
            stream.close();
        } catch (IOException e) {
            System.err.println("Error closing stream: " + e.getMessage());
        }
    }
}
