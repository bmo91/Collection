package org.academiadecodigo.popstars.client;

import java.io.*;
import java.net.Socket;

public class ChatClient {

   private Socket chatServer;

   public ChatClient(String host, int port){

       try {
           System.out.println("Connecting to server...");
           chatServer = new Socket(host, port);
           System.out.println("Connected!");
       } catch (IOException e) {
           e.printStackTrace();
       }
   }

   public void start(){

       try {
           BufferedReader chatInput = new BufferedReader(new InputStreamReader(chatServer.getInputStream()));
           Thread thread = new Thread(new KeyboardHandler(chatServer));
           thread.start();

           while(true) {
               String feedback = chatInput.readLine();

               if(feedback == null){
                   break;
               }
               System.out.println(feedback);
           }

       } catch (IOException e) {
           e.printStackTrace();
       } finally {
           closeSocket(chatServer);
       }
   }

   private void closeSocket(Socket chatServer){
       if(chatServer == null){
           return;
       }

       try {
           chatServer.close();
       } catch (IOException e) {
           e.printStackTrace();
       }
   }

    public static void main(String[] args) {
        ChatClient chatClient = new ChatClient("localhost", 8002);
        chatClient.start();
    }
}
