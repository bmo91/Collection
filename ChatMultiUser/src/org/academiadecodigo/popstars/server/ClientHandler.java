package org.academiadecodigo.popstars.server;

import java.io.*;
import java.net.Socket;

public class ClientHandler implements Runnable {

    private BufferedReader is = null;
    private PrintWriter os = null;
    private Socket client;
    private ChatServer server;


    public ClientHandler(Socket client, ChatServer server) {
        this.client = client;
        this.server = server;
    }

    @Override
    public void run() {

        try {

            is = new BufferedReader(new InputStreamReader(client.getInputStream()));
            os = new PrintWriter(client.getOutputStream(), true);
            os.println("Enter your name...");
            String name = is.readLine();

            if (name == null) {
                server.remove(this);
                return;
            }

            name = name.trim();
            os.println("Hello, " + name + " to our chat! To leave enter /quit in a new line");

            server.broadcast("New user " + name + " has entered the chat.");


            while (true) {
                String line = is.readLine();

                if (line == null || line.startsWith("/quit")) {
                    break;
                }

                server.broadcast("*" + name + ": " + line);
            }

            server.broadcast("The user " + name + " is leaving");
            os.println("Ciao, " + name);

            server.remove(this);
            client.close();


        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void send(String message) {
        os.println(message);
    }
}
