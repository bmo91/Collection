package org.academiadecodigo.popstars.server;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;


public class ChatServer {

    private ServerSocket socket;

    final int maxClientsCount = 10;
    private final List<ClientHandler> clients;


    public ChatServer(int port) {
        System.out.println("Waiting for connection..." + port);

        try {
            socket = new ServerSocket(port);
            System.out.println("Connected to " + port);
        } catch (IOException e) {
            e.printStackTrace();
        }

        clients = Collections.synchronizedList(new LinkedList<>());
    }

    public void start() {

        try {

            while (true) {
                System.out.println("Waiting for client...");
                //BLOCKING until new client connects!
                Socket client = socket.accept();
                System.out.println("Client " + client.getInetAddress() + " has joined!");

                if (clients.size() >= maxClientsCount) {
                    reject(client);
                    continue;
                }

                ClientHandler clientHandler = new ClientHandler(client, this);
                Thread thread = new Thread(clientHandler);
                thread.start();

                clients.add(clientHandler);

            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void reject(Socket socket) {
        try {
            PrintStream os = new PrintStream(socket.getOutputStream(), true);
            os.println("Server too busy. Try later");
            socket.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void broadcast(String msg) {
        synchronized (clients) {
            for (ClientHandler handler : clients) {
                handler.send(msg);
            }
        }
    }

    public void remove(ClientHandler client) {
        clients.remove(client);
    }


    public static void main(String[] args) {
        ChatServer chatServer = new ChatServer(8002);
        chatServer.start();
    }
}

