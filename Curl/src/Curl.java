import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;

public class Curl {

    public static void main(String[] args) {

        String link = "http://news.mit.edu/2018/mit-opencourseware-opens-new-career-trent-parker-0522";

        try {
            URL url = new URL(link);

            BufferedReader urlReader = new BufferedReader(new InputStreamReader(url.openStream()));
            System.out.println("Connecting to website...");

            url.openStream();

            while(true){
                 String line = urlReader.readLine();
                 System.out.println(line);
                 if(line == null){
                     return;
                 }
            }

        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
