public class Main {
    public static void main(String[] args) {
        Range range = new Range(10, 20);

        for (Integer i : range) {
            System.out.println(i);
        }

        range.setReversed(boolean reversed);
        for(Integer i : range){
            System.out.println(i);
        }
    }
}

/*
{ Exercise }
Bi-directional integer Range class
A Range (outer) class that implements Iterable
A BiDirectionalIterator anonymous class
 */