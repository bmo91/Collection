import java.util.Iterator;
import java.util.LinkedList;
import java.util.NoSuchElementException;
import java.util.List;

public class Range implements Iterable<Integer>{

    private int min;
    private int max;
    private List<Integer> removed;
    private boolean reversed;

    public Range(int min, int max){
        this.min = min;
        this.max = max;
        removed = new LinkedList<>();
    }

    @Override
    public Iterator<Integer> iterator(){
        return new BIterator();
    }

    public void setReversed(boolean reversed){
        this.reversed = reversed;
    }

    public class BIterator implements Iterator<Integer> {
        private int current;

        @Override
        public boolean hasNext(){
            return current + 1 < max;
        }

        @Override
        public Integer next(){
            if (current + 1 >= max){
                throw new NoSuchElementException();
            } current++;
            return min + current;
        }
    }

    public class MyIterator implements Iterator<Integer>{
        private int current;

        @Override
        public boolean hasNext(){
            while(removed.contains(current - 1)){
                current--;
            } return current  > min;
        }

        @Override
        public Integer next(){
            if(!hasNext()){
                throw new NoSuchElementException();
            } current --;
            return current;
        }
    }
}
