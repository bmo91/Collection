package org.academiadecodigo.chat;

import java.io.BufferedReader;
import java.io.PrintWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Socket;
import java.net.UnknownHostException;


public class Client {

    public static void main(String[] args) {

        // STEP1: Get the host and the port from the command-line
        String host = "localhost";
        int port = 8000;
        Socket clientSocket = null;

        try {
            // STEP2: Open a client socket, blocking while connecting to the server
            System.out.println("Waiting for connection...");
            clientSocket = new Socket(host, port);

            // STEP3: Setup input and output streams
            PrintWriter out = new PrintWriter(clientSocket.getOutputStream(), true);
            BufferedReader in = new BufferedReader(new InputStreamReader(System.in));


            // STEP4: Read from/write to the stream
            while(!clientSocket.isClosed())
                System.out.println("Reading line...");
                String line = in.readLine();
                while (line != null){
                    out.print(line);
                    out.flush();
                    System.out.println("Reading line...");
                    line = in.readLine();
                }
            } catch (UnknownHostException e1) {
            e1.printStackTrace();
        } catch (IOException e1) {
            e1.printStackTrace();
        }
    }
}
