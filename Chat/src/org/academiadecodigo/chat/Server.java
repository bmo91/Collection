package org.academiadecodigo.chat;

import java.io.*;
import java.net.*;

public class Server {

    public static void main(String[] args) {

        // STEP1: Get parameters from command line arguments
        int port = 8000;
        Socket clientSocket = null;

        try {
                // STEP2: Bind to local port and block while waiting for client connections
                ServerSocket serverSocket = new ServerSocket(port);
                System.out.println("Waiting for connection...");
                clientSocket = serverSocket.accept();

                // STEP3: Setup input and output streams
                BufferedReader in = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));


                // STEP4: Read from/write to the stream

                while(!clientSocket.isClosed()){
                    String message = in.readLine();

                    if(message == null){
                        System.out.println("Connection terminated...");
                        break;
                    }

                    String response = clientSocket.getInetAddress() + " " + message;
                    System.out.println(response);
                }

            } catch (IOException e) {
                e.printStackTrace();
            } finally {
            // STEP6: Close the sockets
                if(clientSocket != null){
                    try {
                        clientSocket.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
        }

        }
}

