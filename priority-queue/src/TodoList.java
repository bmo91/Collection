import java.util.*;

public class TodoList {
     private Queue<TodoItem> queue = new PriorityQueue<>();



    public void add(Importance importance, int priority, String item) {
        TodoItem addThis = new TodoItem(importance, priority, item);
        queue.add(addThis);
    }

    public TodoItem remove() {
        return queue.remove();
    }

    public boolean isEmpty() {
        return queue.isEmpty();
    }
}
