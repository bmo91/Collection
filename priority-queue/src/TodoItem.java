public class TodoItem implements Comparable<TodoItem>{

    Importance importance;
    private String item;
    private int priority;

    TodoItem(Importance importance, int priority, String item){
        super();
        this.item = item;
        this.priority = priority;
        this.importance = importance;
    }

    @Override
    public int compareTo(TodoItem compareTodoItem){
        if(this.importance == compareTodoItem.importance){
            return this.priority - compareTodoItem.priority;
        }
        return this.importance.ordinal() - compareTodoItem.importance.ordinal();
    }

    @Override
    public String toString() {
        return "TodoItem{" +
                "importance=" + importance +
                ", item='" + item + '\'' +
                ", priority=" + priority +
                '}';
    }
}
