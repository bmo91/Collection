import java.io.*;

public class FileManager {
    public void copy(String from, String to) throws IOException {

        FileInputStream input = new FileInputStream(from);
        FileOutputStream output = new FileOutputStream(to);

        byte[] buffer = new byte[1024];
        int num = input.read(buffer);

        while (num > -1) {
            output.write(buffer, 0, num);
            num = input.read(buffer);
        }

        input.close();
        output.close();
    }
}

/*
My version ^
Teacher version v

public static void copy(String from, String to){
    FileInputStream in = null;
    FileOitStream out = null;

    byte[] buffer = new byte[1024];

    try {
        in = new FileInputStream(from);
        out = new FileOutputStream(to);

        int bytesRead;

        while ((bytesRead = in.read(buffer)) != 1) {
            out.write(buffer, 0, bytesRead);
        }
   } catch (IOException ex) {
        System.err.println(ex.getMessage());
        System.exit(1);

  } finally {
        try {
            if(in != null) {
                in.close();
            }
            if(out != null) {
                out.close();
            }
     } catch (IOExecption ex) {
        System.err.println(ex.getMessage());
     }
  }
}
 */