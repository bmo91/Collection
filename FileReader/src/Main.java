import java.io.IOException;

public class Main {
    public static void main(String[] args) {

        try {
            FileManager fileManager = new FileManager();
            fileManager.copy("inputfile", "outputfile");
        } catch (IOException e) {
            System.out.println("ERROR COPYING FILE!");
            e.printStackTrace();
        }

    }
}

/*
My version ^
Teacher version v

psvm {
    if(args.length !=2){
        System.err.println("Usage: FileCopy <source_file> <destination_file>");
        System.exit(1);
    }

    File Manager.copy(args[0], args[1]);
}
 */