import java.util.*;

import java.util.Iterator;

public class UniqueWord implements Iterable<String> {

    private Set<String> uniqueWords = new HashSet<String>();

    public UniqueWord(String inString){
        Collections.addAll(uniqueWords, inString.split(" "));
    }

    @Override
    public Iterator<String> iterator() {
        return uniqueWords.iterator();
    }


}
