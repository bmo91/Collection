# Collection
...of exercises done in a 14-Week Intensive && Immersive Java && JavaScript FullStack Coding Bootcamp
@<Academia de Código_> 15th bootcamp, PopStars class

Syllabus:

Module 1 - Programming in Java
Introduction to Computer Science and Programming
Version Control Systems
Java Programming Language
Object Oriented Programming

Module 2 - Advanced Concepts and Tools
Documentation and Code Conventions
Network Programming
Concurrent Programming
Software Engineering
Build Systems
Testing
Debugging

Module 3 - Databases, Frameworks and Web Development
Relational Database Management Systems
Java Database Connectivity
Java Persistence
Spring Framework
Java Web Programming

Module 4 - JavaScript
JS Fundamentals
Frontend Web Development
jQuery Library
