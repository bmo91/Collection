import org.academiadecodigo.simplegraphics.graphics.Color;
import org.academiadecodigo.simplegraphics.graphics.Rectangle;

public class Cell {

    private Rectangle rectangle;

    public Cell(int col, int row, Grid grid) {
        rectangle = grid.createRectangle(col, row);
        rectangle.setColor(Color.BLACK);
        rectangle.draw();
    }

    public void paint() {
        rectangle.fill();
    }

    public void clear() {
        rectangle.draw();
    }

    public boolean isPainted() {
        return rectangle.isFilled();
    }

    @Override
    public String toString() {
        return isPainted() ? "#" : "_";
    }

    public void fromString(String str) {
        if (str.equals("#")) {
            paint();
        } else {
            clear();
        }
    }

    public void toggle() {
        if (isPainted()) {
            clear();
        } else {
            paint();
        }
    }


}
//Teacher version
//My version v
/*
private int posX;
    private int posY;

    private Rectangle rectangle;
    private int col;
    private int row;

    private boolean isPainted;

    public Cell(int col, int row){
        this.col = col;
        this.row = row;

        this.posX = this.col * Grid.CELL_SIZE;
        this.posY = this.row * Grid.CELL_SIZE;

        this.rectangle = new Rectangle(this.posX, this.posY, Grid.CELL_SIZE, Grid.CELL_SIZE);

      //  this.rectangle =  new Rectangle((row * Grid.CELL_SIZE) + Grid.PADDING, (col*Grid.CELL_SIZE) + Grid.PADDING, Grid.CELL_SIZE, Grid.CELL_SIZE);
        this.rectangle.setColor(Color.LIGHT_GRAY);
        this.show();
       // rectangle.draw();
    }

    public void togglePaint(){
        if(!isPainted){
            paint();
        } else {
            unPaint();
        }
    }

    public int getCol(){return  col;}

    public int getRow(){return row;}

    public int getColToX(){return this.posX * Grid.CELL_SIZE;}

    public int getRowToY(){return  this.posY * Grid.CELL_SIZE;}

    public boolean getIsPainted(){ return isPainted;}

    public void show(){this.rectangle.draw();}

    public void paint(){ rectangle.fill(); }

    public void unPaint(){ rectangle.draw(); }

   /* public boolean isFilled(){
        return rectangle.isFilled();
    }*/