import org.academiadecodigo.simplegraphics.graphics.Rectangle;

import java.util.ArrayList;

public class Grid {

    private final int PADDING = 10;

    private int cols;
    private int rows;
    private int cellSize;
    private Cell[][] cells;

    public Grid(int cols, int rows, int cellSize) {
        this.cols = cols;
        this.rows = rows;
        this.cellSize = cellSize;

        this.cells = new Cell[cols][rows];
        initCells();
    }

    public void paint(int col, int row) {
        this.cells[col][row].paint();
    }

    public void clear(int col, int row) {
        this.cells[col][row].clear();
    }

    public void fromString(String grid) {
        int col = 0;
        int row = 0;

        for (int c = 0; c < grid.length(); c++) {
            System.out.println("Iterating through "+ col + " "  + row);

            if (grid.charAt(c) != '\n') {
                cells[col][row].fromString(String.valueOf(grid.charAt(c)));
                col++;
            } else {
                col = 0;
                row++;
            }
        }
    }


    @Override
    public String toString() {
        String grid = "";

        for (int row = 0; row < rows; row++) {
            for (int col = 0; col < cols; col++) {
                grid += cells[col][row].toString();
            }

            grid += "\n";
        }

        return grid;
    }

    public int getCols() {
        return cols;
    }

    public int getRows() {
        return rows;
    }

    public Rectangle createRectangle(int col, int row) {
        int x = PADDING + col * cellSize;
        int y = PADDING + row * cellSize;

        return new Rectangle(x, y, cellSize, cellSize);
    }

    private void initCells() {
        for (int col = 0; col < cols; col++) {
            for (int row = 0; row < rows; row++) {
                this.cells[col][row] = new Cell(col, row, this);
            }
        }
    }

    public void toggle(int col, int row) {
        cells[col][row].toggle();
    }

}

//Teacher version ^
//My version v
/*
public class Grid {
    private int posX;
    private int posY;

    public static final int PADDING = 10;
    public static final int CELL_SIZE = 32;
    public static final int GRID_SIZE = 30;

    private Rectangle grid;
    public ArrayList<Cell> cellArrayList;


    public Grid(int posX, int posY){
        this.posX = posX;
        this.posY = posY;

        this.grid = new Rectangle(this.posX, this.posY, this.GRID_SIZE * CELL_SIZE, this.GRID_SIZE*CELL_SIZE);
        cellArrayList = new ArrayList<>();

        for(int i = 0; i < GRID_SIZE; i++){ //ROWS
            for(int j = 0; j < GRID_SIZE; j++){ //COLS
                Cell cell = new Cell(j, i);
                cellArrayList.add(cell);
            }
        }
    }

    public void checkCursorInCell(int col, int row){
        for(Cell c : cellArrayList){
            if(c.getCol() == col && c.getRow() == row){
                c.togglePaint();
            }
        }
    }

}
*/

