import java.io.IOException;
import java.io.*;

public class FileHelper {

    public static void save(String file_name, String data) throws IOException {
        PrintWriter writer = new PrintWriter(new FileWriter(file_name), true);
        writer.print(data);

        closeStream(writer);
    }


    public static String read(String file_name) throws FileNotFoundException {
        String data = "";
        BufferedReader reader = new BufferedReader(new FileReader(file_name));

        try {
            String line = reader.readLine();

            while (line != null) {
                data += line + "\n";
                line = reader.readLine();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        return data;
    }

    private static void closeStream(Closeable stream) {
        try {
            stream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


}
//Teacher version ^
//My version v
/*
/*  private String filePath = ".resources/map.txt";

    public String loadMap() throws IOException{
        BufferedReader bufferedReader = new BufferedReader(new FileReader(filePath));
        String mapLoaded = bufferedReader.readLine();
        bufferedReader.close();
        return mapLoaded;
    }

    public void saveMap(String mapToSave) throws IOException {
        BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(filePath));
        try {
            bufferedWriter.write(mapToSave);
            bufferedWriter.flush();
        } catch (IOException e){
            e.printStackTrace();
        } finally {
            bufferedWriter.close();
        }
    }*/


