
import org.academiadecodigo.simplegraphics.graphics.Color;
import org.academiadecodigo.simplegraphics.graphics.Rectangle;

public class Cursor {

    private Rectangle rectangle;
    private int col;
    private int row;
    private int gridCols;
    private int gridRows;

    public Cursor(Grid grid) {
        col = 0;
        row = 0;
        gridCols = grid.getCols();
        gridRows = grid.getRows();

        rectangle = grid.createRectangle(col, row);
        rectangle.setColor(Color.GREEN);
        rectangle.fill();
    }

    public void move(Direction direction) {

        switch (direction) {
            case UP:
                moveUp();
                break;
            case DOWN:
                moveDown();
                break;
            case LEFT:
                moveLeft();
                break;
            case RIGHT:
                moveRight();
                break;
        }
    }

    private void moveRight() {
        if (col == gridCols - 1) {
            return;
        }

        rectangle.translate(rectangle.getWidth(), 0);
        col++;
    }

    private void moveLeft() {
        if (col == 0) {
            return;
        }

        rectangle.translate(-rectangle.getWidth(), 0);
        col--;
    }

    private void moveDown() {
        if (row == gridRows - 1) {
            return;
        }

        rectangle.translate(0, rectangle.getHeight());
        row++;
    }

    private void moveUp() {
        if (row == 0) {
            return;
        }

        rectangle.translate(0, -rectangle.getHeight());
        row--;
    }

    public int getCol() {
        return col;
    }

    public int getRow() {
        return row;
    }

}
//Teacher version ^
//My version v


/*
public class Cursor {

    private Rectangle rectangle;
    private int posX;
    private int posY;
    private int col;
    private int row;
    private boolean isPainted;

    public Cursor(int col, int row) {
        this.col = col;
        this.row = row;
        this.posX = this.col * Grid.CELL_SIZE;
        this.posY = this.row * Grid.CELL_SIZE;
        this.isPainted = false;

        this.rectangle= new Rectangle(this.posX, this.posY, Grid.CELL_SIZE, Grid.CELL_SIZE);
        this.rectangle.setColor(Color.BLACK);
        this.rectangle.fill();

       // inputEvent();
    }

    public int getCol(){return this.col;}
    public int getRow(){return this.row;}

    public void moveUp(){
        if(this.row > 0){
            this.row = this.row - 1;
            System.out.println("row " + this.row);
            rectangle.translate(0, -Grid.CELL_SIZE);
        }
    }

    public void moveLeft(){
        if(this.col > 0){
            this.col = this.col - 1;
            System.out.println("col " + this.row);
            rectangle.translate(-Grid.CELL_SIZE,0);
        }
    }

    public void moveDown(){
        if(this.row > Grid.CELL_SIZE - 1){
            this.row = this.row + 1;
            System.out.println("row " + this.row);
            rectangle.translate(0, Grid.CELL_SIZE);
        }
    }

    public void moveRight(){
        if(this.col < Grid.CELL_SIZE - 1){
            this.col = this.col + 1;
            System.out.println("col " + this.col);
            rectangle.translate(Grid.CELL_SIZE, 0);

        }
    }

    public int getColToX(){return (this.getCol() * Grid.CELL_SIZE); }
    public int getRowToY(){return (this.getRow() * Grid.CELL_SIZE);}

    public boolean getIsPainted(){return isPainted;}
    public void show(){this.rectangle.draw();}

    public void paint(){
        this.rectangle.setColor(Color.ORANGE);
        this.rectangle.fill();
        this.isPainted = true;
    }

    public void unPaint(){
        this.rectangle.setColor(Color.BLACK);
        this.rectangle.draw();
        this.isPainted = false;
    }


}
*/
