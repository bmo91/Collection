

import org.academiadecodigo.simplegraphics.keyboard.Keyboard;
import org.academiadecodigo.simplegraphics.keyboard.KeyboardEvent;
import org.academiadecodigo.simplegraphics.keyboard.KeyboardEventType;
import org.academiadecodigo.simplegraphics.keyboard.KeyboardHandler;

import java.io.FileNotFoundException;
import java.io.IOException;

public class MapEditor {

    private final String FILE_NAME = "saved_map";

    private Grid grid;
    private Cursor cursor;

    public MapEditor(int cols, int rows, int cellSize) {
        grid = new Grid(cols, rows, cellSize);
        cursor = new Cursor(grid);
    }


    public void toggle() {
        grid.toggle(cursor.getCol(), cursor.getRow());
    }

    public void paint() {
        grid.paint(cursor.getCol(), cursor.getRow());
    }

    public void clear() {
        grid.clear(cursor.getCol(), cursor.getRow());
    }

    public void moveCursor(Direction direction) {
        cursor.move(direction);
    }

    public void save() {
        try {
            FileHelper.save(FILE_NAME, grid.toString());
        } catch (IOException e) {
            System.err.println("Problems saving to " + FILE_NAME);
        }
    }

    public void load() {
        try {
            String gridText = FileHelper.read(FILE_NAME);
            grid.fromString(gridText);
        } catch (FileNotFoundException e) {
            System.err.println("File " + FILE_NAME + " not found.");
        }

    }

}

//Teacher version
//My version v
/*
public class MapEditor implements KeyboardHandler{

 private FileHelper fileHelper;
    private Cursor cursor;
    private Grid grid;

    private char[] charRead;

    String mapLoaded= "";
    String mapToSave;

    public MapEditor(){
        fileHelper = new FileHelper();
        loadMap();

        grid = new Grid(Grid.PADDING, Grid.PADDING);
        charRead = new char[Grid.GRID_SIZE * Grid.GRID_SIZE];

        translateMapLoad();
        initKeyboard();
        cursor = new Cursor(0,0);
    }

    public void loadMap(){
        try{
            mapLoaded = this.fileHelper.loadMap();

        } catch (IOException e){
          e.printStackTrace();
        } if(mapLoaded.length() != (Grid.GRID_SIZE * Grid.CELL_SIZE)){
            mapLoaded = "";
            for(int i = 0; i < Grid.GRID_SIZE * Grid.CELL_SIZE; i++){
                mapLoaded = mapLoaded + "0";
            }
        }
    }

    public void saveMap(){
        String result = "";
        char one = '1';
        char zero = '0';

        for(int i = 0; i < charRead.length; i++){
            if(grid.cellArrayList.get(i).getIsPainted()){
                result = result + one;
            } else {
                result = result + zero;
            }
        }

        mapToSave = result;
        try {
            fileHelper.saveMap(mapToSave);
        } catch (IOException e){
            e.printStackTrace();
        }
    }

    public void translateMapLoad(){
        for(int i = 0; i < charRead.length; i++){
            charRead[i] = mapLoaded.charAt(i);
        }

        for(int i = 0; i < charRead.length; i++){
            if(charRead[i] == '0'){
                grid.cellArrayList.get(i).unPaint();
            }

            if(charRead[i] == '1'){
                grid.cellArrayList.get(i).paint();
            }
        }
    }

    public void initKeyboard(){
        Keyboard keyboard = new Keyboard(this);

        KeyboardEvent upEvent = new KeyboardEvent();
        KeyboardEvent rightEvent = new KeyboardEvent();
        KeyboardEvent downEvent = new KeyboardEvent();
        KeyboardEvent leftEvent = new KeyboardEvent();
        KeyboardEvent paintEvent = new KeyboardEvent();
        KeyboardEvent loadEvent = new KeyboardEvent();
        KeyboardEvent saveEvent = new KeyboardEvent();

        upEvent.setKey(KeyboardEvent.KEY_UP);
        upEvent.setKeyboardEventType(KeyboardEventType.KEY_PRESSED);

        downEvent.setKey(KeyboardEvent.KEY_DOWN);
        downEvent.setKeyboardEventType(KeyboardEventType.KEY_PRESSED);

        rightEvent.setKey(KeyboardEvent.KEY_RIGHT);
        rightEvent.setKeyboardEventType(KeyboardEventType.KEY_PRESSED);

        leftEvent.setKey(KeyboardEvent.KEY_LEFT);
        leftEvent.setKeyboardEventType(KeyboardEventType.KEY_PRESSED);

        paintEvent.setKey(KeyboardEvent.KEY_SPACE);
        paintEvent.setKeyboardEventType(KeyboardEventType.KEY_PRESSED);

        loadEvent.setKey(KeyboardEvent.KEY_L);
        loadEvent.setKeyboardEventType(KeyboardEventType.KEY_PRESSED);

        saveEvent.setKey(KeyboardEvent.KEY_S);
        saveEvent.setKeyboardEventType(KeyboardEventType.KEY_PRESSED);

        keyboard.addEventListener(upEvent);
        keyboard.addEventListener(downEvent);
        keyboard.addEventListener(leftEvent);
        keyboard.addEventListener(rightEvent);
        keyboard.addEventListener(saveEvent);
        keyboard.addEventListener(loadEvent);
    }

    public void checkCellCursorsIPainted(){
        grid.checkCursorInCell(cursor.getCol(), cursor.getRow());
    }

    @Override
    public void keyPressed(KeyboardEvent keyboardEvent) {
        if(keyboardEvent.getKey() == keyboardEvent.KEY_UP){
            System.out.println("UP");
            cursor.moveUp();
        }

        if(keyboardEvent.getKey() == keyboardEvent.KEY_DOWN){
            System.out.println("Down");
            cursor.moveDown();
        }

        if(keyboardEvent.getKey() == keyboardEvent.KEY_LEFT){
            System.out.println("Left");
            cursor.moveLeft();
        }

        if(keyboardEvent.getKey() == keyboardEvent.KEY_RIGHT){
            System.out.println("Right");
            cursor.moveRight();
        }

        if(keyboardEvent.getKey()== keyboardEvent.KEY_L){
            System.out.println("Load");
            loadMap();
        }

        if(keyboardEvent.getKey()==keyboardEvent.KEY_S){
            System.out.println("Save");
            saveMap();
        }

        if(keyboardEvent.getKey() == keyboardEvent.KEY_SPACE){
            System.out.println("Paint");
        }

        grid.checkCursorInCell(cursor.getCol(), cursor.getRow());
    }

    @Override
    public void keyReleased(KeyboardEvent keyboardEvent) {}
 */