import java.io.IOException;
import java.net.InetAddress;
import java.net.UnknownHostException;

public class DNSResolve {
    public static void main(String[] args) {
        if(args.length == 0){
            throw new IllegalArgumentException("You need to inset a host as an argument");
        }

        String host = args[0];
        String address = DNSResolve.resolve(host);
        System.out.println("Address is " + address);
    }

    public static String resolve(String host){
        String address =  null;

        try {
            InetAddress inetAddress = InetAddress.getByName(host);
            address = inetAddress.getHostAddress();
            System.out.println("inetAdress is " + inetAddress);

            System.out.println("Testing reachbility..");
            boolean reachable = inetAddress.isReachable(2000);
            System.out.println("Reachable? " + (reachable ? "YES!" : "NO"));

        } catch (UnknownHostException e)      {

        } catch (IOException e) {
            e.printStackTrace();
        }

        return address;
    }

}
