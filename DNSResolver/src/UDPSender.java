import java.io.IOException;
import java.net.*;

public class UDPSender {
    public static void main(String[] args) {

        DatagramSocket socket = null;

        if(args.length < 2){
            throw new IllegalArgumentException("Usage: <Hostname> <port>");
        }

        String host = args[0];
        int port = Integer.parseInt(args[1]);
        String message = args[2];

        try {
            InetAddress inetAddress = InetAddress.getByName(host);
            socket = new DatagramSocket();

            byte[] buffer = message.getBytes();
            DatagramPacket packet = new DatagramPacket(buffer, buffer.length, inetAddress, port);

            socket.send(packet);
        } catch (UnknownHostException e) {
            e.printStackTrace();
        } catch (SocketException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
            finally {
            if(socket != null){
                socket.close();
            }
        }
    }
}
