package org.academiadecodigo.carcrash.field;

public enum Directions{
    UP,
    DOWN,
    LEFT,
    RIGHT
}
