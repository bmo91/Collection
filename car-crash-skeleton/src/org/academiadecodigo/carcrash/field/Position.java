package org.academiadecodigo.carcrash.field;

public class Position {

    private int col;
    private int row;

    public Position(int col, int row){
        this.col = col;
        this.row = row;
    }

    //MoveRight(width) - increment
    public void moveRight(){
        if(col + 1 < Field.getWidth()){
            col++;
        } else col = Field.getWidth() - 1;
    }
    //MoveLeft - decrement
    public void moveLeft(){
        if(col - 1 > 0){
            col--;
        } else col = 0;
    }

    //MoveUp(height) - decrement
    public void moveUp(){
        if(row + 1 < Field.getHeight()){
            row++;
        } else row = Field.getHeight() - 1;
    }
    //MoveDown - increment
    public void moveDown(){
        if(row - 1 > 0){
            row--;
        } else row = 0;
    }

    public boolean equals(Position other) {
        return col == other.col &&
                row == other.row;
    }


    public int getCol() {
        return col;
    }

    public int getRow() {
        return row;
    }
}
