package org.academiadecodigo.carcrash.cars;

import org.academiadecodigo.carcrash.field.Directions;
import org.academiadecodigo.carcrash.field.Field;
import org.academiadecodigo.carcrash.field.Position;

abstract public class Car {

    /** The position of the car on the grid */
    private Position pos; //random;
    private boolean crashed;
    private int slowDown;
    private Directions dir;


    //random positions of cars
    public Car(){
        int col = (int)(Math.random()*Field.getWidth() + 1);
        int row = (int)(Math.random()*Field.getHeight() + 1);
        pos = new Position(col, row);
        dir = Directions.values()[(int)(Math.random()*Directions.values().length)];
        slowDown = 0;
    }

    public void move(){
        if(!this.isCrashed()){
            Directions newDir = chooseDir();

            switch (newDir){
                case UP:
                    getPos().moveUp();
                    dir = (Directions.UP);
                    break;
                case DOWN:
                    getPos().moveDown();
                    dir = (Directions.DOWN);
                    break;
                case RIGHT:
                    getPos().moveRight();
                    dir = (Directions.RIGHT);
                    break;
                case LEFT:
                    getPos().moveLeft();
                    dir = (Directions.LEFT);
                    break;
            }
        }
    }

    private Directions chooseDir(){
        Directions newDir = dir;
        if((int)(Math.random()*41) > 30){
            newDir = Directions.values()[(int)(Math.random()*Directions.values().length)];
        } return newDir;
    }

    public Position getPos() {
        return pos;
    }

    public boolean isCrashed() {
        return crashed;
    }

    public void crashCar(){
        if(slowDown == 0){
            crashed = true;
        }
    }
}
