package org.academiadecodigo.carcrash.cars;

public class CarFactory {

    public static Car getNewCar() {

        int temp = (int) (Math.random() * CarType.values().length);
        switch (CarType.values()[temp]) {
            case BEETLE:
                return new Beetle();
            case MUSTANG:
                return new Mustang();
            case FIAT:
                return new Fiat();
            default:
                return new Fiat();
        }

    }
}
