package org.academiadecodigo.carcrash.cars;

public class Fiat extends Car {
    private int slowingFactor;

    public void move(){
        if(slowingFactor == 0){
            super.move();
            slowingFactor = 2;
        } else slowingFactor--;
    }

    @Override
    public String toString(){
        return "F";
    }
}