package org.academiadecodigo.carcrash.cars;


public class Beetle extends Car {
    private int slowingFactor;

    public void move(){
        if(slowingFactor == 0){
            super.move();
            slowingFactor = 3;
        } else slowingFactor--;
    }

    @Override
    public String toString(){
        return "B";
    }
}
