package org.academiadecodigo.carcrash;

import org.academiadecodigo.carcrash.cars.Car;
import org.academiadecodigo.carcrash.cars.CarFactory;
import org.academiadecodigo.carcrash.field.Field;

public class Game {

    public static final int MANUFACTURED_CARS = 10;

    /** Container of Cars */
    private Car[] cars;

    /** Animation delay */
    private int delay;

    public Game(int cols, int rows, int delay) {

        Field.init(cols, rows);
        this.delay = delay;

    }

    /**
     * Creates a bunch of cars and randomly puts them in the field
     */
    public void init() {

        cars = new Car[MANUFACTURED_CARS];
        for (int i = 0; i < cars.length; i++) {
            cars[i] = CarFactory.getNewCar();
        }

        Field.draw(cars);

    }

    /**
     * Starts the animation
     *
     * @throws InterruptedException
     */
    public void start() throws InterruptedException {

        while (true) {

            // Pause for a while
            Thread.sleep(delay);

            // Move all cars
            moveAllCars();

            // check for collisions
            crashCars();

            // Update screen
            Field.draw(cars);

        }

    }

    private void moveAllCars() {
        for(Car c : cars){
            c.move();
        }

    }

    //Cars crash when....
    public void crashCars() {
        for (Car c1 : cars) {
            for (Car c2 : cars) {
                if(c1 == c2){
                    continue;
                }
                if (c1.getPos().equals(c2.getPos())) {
                    c1.crashCar();
                    c2.crashCar();
                }
            }
        }
    }

}
