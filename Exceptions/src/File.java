/*
getName() -- String
 */

public class File {

    public String fileName;

    public File(String fileName){
        this.fileName = fileName;
    }

    public String getFileName() {
        return fileName;
    }
}
