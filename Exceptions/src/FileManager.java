/*
login() -- void
logout() -- void
getFile(String) -- File
createFile(String) -- void
 */

import exceptions.FileNotFoundException;
import exceptions.NotEnoughPermissionsException;
import exceptions.NotEnoughSpaceException;


public class FileManager {
    private boolean loggedIn = false;
    private File[] fileSpace;
    private int fileCounter =0;

    public FileManager(int maxFiles){
        this.fileSpace =new File[maxFiles];
    }

    public void login(){ this.login(); }

    public void logout(){ this.logout(); }

    public File getFile(String fileName) throws FileNotFoundException {
           for(File file : fileSpace){
            if (file == null) {
                continue;
            }
            if (file.getFileName().equals(fileName)) {
                return file;
            }
        }
        throw new FileNotFoundException();
    }

    public void createFile(String fileName) throws NotEnoughPermissionsException, NotEnoughSpaceException {
        if(!loggedIn) {
            throw new NotEnoughPermissionsException();
        }
        if(fileCounter == fileSpace.length){
            throw new NotEnoughSpaceException();
        }
        fileSpace[fileCounter] = new File(fileName);
            fileCounter++;
    }
}
