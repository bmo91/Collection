public class Genie {
    //max of wishes
    private int limit;
    //total wishes
    private int granted;

    public int getLimit() {
        return limit;
    }

    //limit if wishes to grant
    public Genie(int limit) {
        limit = limit;
    }


    //Ask to grant a wish
    public boolean grantWish() {
        if (canGrantWish()) {
            doGrantWish();
            return true;
        }
        return false;
    }

    //Can genie grant?
    public boolean canGrantWish() {
        return granted < limit;
    }

    //Do the granting
    public void doGrantWish() {
        granted++;
        System.out.println("Your wish was granted, bitch!");
    }

    //limit of wishes granted
    public int getGrantedWishes() {
        return granted;
    }
}

