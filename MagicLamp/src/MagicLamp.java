/*returns a genie
max of genie
when recycled(), it goes back to the start
compare to other lamps
after going through all genies, recycles
Every even rub creates a Happy Genie
Every odd rub creates a Grumpy Genie
Only creates, doesn't keep them*/

public class MagicLamp {
    //max of wishes
    private int limit;
    //total rubs
    private int totalRubs;
    //rubs since recharges
    private int rubs;
    //nº recharges
    private int demons;

    //how many genies
    public MagicLamp(int limit){
        this.limit = limit;
    }

    public int getLimit(){
        return limit;
    }

    public int getRubs(){
        return rubs;
    }

    public int getTotalRubs(){
        return totalRubs;
    }
    public int getGenies(){
        return getLimit() - getRubs();
    }
    public int getDemons(){
        return demons;
    }

    //Ask lamp for a genie
    public Genie rub(int wishes) {

        if (rubs < limit) {

            rubs++;
            totalRubs++;

            if (totalRubs % 2 == 0) {
                return new HappyGenie(wishes);
            }
                return new GrumpyGenie(wishes);
        }
            return new RecyclableDemon(wishes);
    }

    //Recharge magic lamp’
    public void feedDemon(RecyclableDemon demon){
        if (!demon.recycled()){
            demon.recycle();
            rubs = 0;
            demons++;
        }
    }

    //default inheritance -
    @Override //?
    public boolean equals(Object o){
        return o instanceof MagicLamp && equals((MagicLamp) o);
    }

    //Compare with other lamps - local method -
    public boolean equals(MagicLamp l){
        return getLimit() == l.getLimit() && getGenies() == l.getGenies() && getDemons() == l.getDemons();
    }
}
