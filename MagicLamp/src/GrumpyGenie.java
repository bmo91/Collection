/*
max wishes
keep score of granted wishes
use inheritance - @Override*/

public class GrumpyGenie extends Genie{
    public GrumpyGenie(int limit){
        super(1);
    }

    @Override //?
    public String toString(){
        return(getGrantedWishes()==1) ? "Grumpy Genie has granted a wish." : "Grumpy genie has a wish to grant.";
    }
}
