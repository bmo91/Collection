/*
Needs to know if it has recycled
can recharge the lamp
use inheritance - @Override*/

public class RecyclableDemon extends Genie {
    //has been recycled?
    private boolean recycled = false;
    //wishes to grant
    public RecyclableDemon(int limit){
        super(limit);
    }

    //@Override?
    @Override
    public boolean canGrantWish() {
        return !recycled;
    }
    public boolean recycled(){
        return recycled;
    }
    public void recycle(){
        recycled = true;
    }

    @Override//?
    public String toString(){
        return recycled ? "Demon has been recycled." : "Recycled demon has granted " + getGrantedWishes() + " wishes";
    }
}
