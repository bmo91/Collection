/*max wishes
keep score of granted wishes
use inheritance - @Override*/

public class HappyGenie extends Genie {

    public HappyGenie(int limit){
        super(limit);
    }

    //return wishes left to grant
    public int getRemainingWishes(){
        return getLimit() - getGrantedWishes();
    }

    @Override//?
    public String toString(){
        return "Happy Genie has granted " + getGrantedWishes() + " wishes and still has " + getRemainingWishes() + " to grant.";
    }
}


