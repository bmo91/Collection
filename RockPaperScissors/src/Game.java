public class Game {
    private Player playerOne;
    private Player playerTwo;

    public Game(Player playerOne, Player playerTwo) {
        this.playerOne = playerOne;
        this.playerTwo = playerTwo;
    }

    public void start(int rounds){
        while (rounds>0){
            playRound();
            rounds--;
        }
        Player winner = playerOne.getScore() > playerTwo.getScore()? playerOne:playerTwo;
    }

    private void playRound(){
        HandType playerOneHand = playerOne.showHand();
        HandType playerTwoHand = playerTwo.showHand();

        if(playerOneHand == playerTwoHand){
            playRound();
            return;
        }
        System.out.println(playerOne.getName()+ " show " + playerOneHand.name().toLowerCase());
        System.out.println(playerTwo.getName() + " show " + playerTwoHand.name().toLowerCase());

        Player winner = playerOneHand.beats(playerTwoHand)?playerOne:playerTwo;

        winner.win();
        System.out.println(winner.getName()+ " wins this round!");
    }
}

// My version
//enum HandType {PAPER, ROCK, SCISSORS}
//    public static void game(String[] args) {
//        Scanner console = new Scanner(System.in);
//        System.out.println("Player 1: Choose Rock, Scissors or Paper.");
//        String player1 = scan.next().toLowerCase();
//        System.out.println("Player 2: Choose Rock , Scissors or Paper.");
//        String player2 = scan.next().toLowerCase();
//
//        HandType player1 = HandType.PAPER;
//        if (player1 == player2) {
//            System.out.println("It's a tie!");
//        } else {
//            switch (player1) {
//                case ROCK:
//                    if (player2 == SCISSORS)
//                        System.out.println("Player 1 Wins!");
//                    else System.out.println("Player 2 Wins!");
//                    break;
//                case PAPER:
//                    if (player2 == ROCK)
//                        System.out.println("Player 1 Wins!");
//                    else System.out.println("Player 2 Wins!");
//                    break;
//                case SCISSORS:
//                    if (player2 == PAPER)
//                        System.out.println("Player 1 Wins!");
//                    else System.out.println("Player 2 Wins!");
//                    break;
//
//            }
//        }
//    }
//    //play again(?)
//    public boolean playAgain() {
//        System.out.print("Do you want to play again? ");
//        String userInput = inputScanner.nextLine();
//        userInput = userInput.toUpperCase();
//        return userInput.charAt(0) == 'Y';
//    }
