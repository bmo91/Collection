import java.util.Scanner;

public class Completo {

    public static void main(String[] args) {

        Scanner console = new Scanner(System.in);

        String value = console.nextLine();

        if (isValid(value)) {
            int Player1 = (int)(Math.random()*3);
            int Player2 = getVal(value);
            System.out.println("You picked "+value);
            System.out.println("The computer picked "+getPlayer1(Player1));
            System.out.println("You "+didPlayer2Win(Player2, Player1));
        }
        else {
            System.out.println("That is not a valid input!");
        }

        console.close();
    }

    public static String getPlayer1(int x) {
        if (x == 0) {
            return "rock";
        }
        if (x == 1) {
            return "paper";
        }
        return "scissors";
    }
    public static String didPlayer2Win(int Player2Value, int Player1Value) {
        if (Player2Value == 0) {
            if (Player1Value != 1) {
                if (Player1Value != 0) {
                    return "WIN";
                }
                return "Tie";
            }
            return "Loose";
        }
        if (Player2Value == 1) {
            if (Player1Value != 2) {
                if (Player1Value != 1) {
                    return "WIN";
                }
                return "Tie";
            }
            return "Loose";
        }
        if (Player2Value == 2) {
            if (Player1Value != 0) {
                if (Player1Value != 2) {
                    return "WIN";
                }
                return "Tie";
            }
            return "Loose";
        }
        return "IDK";
    }

    public static boolean isValid(String string) {
        if (string.equalsIgnoreCase("rock")) {
            return true;
        }
        if (string.equalsIgnoreCase("paper")) {
            return true;
        }
        return string.equalsIgnoreCase("scissors");
    }

    public static int getVal(String string) {
        if (string.equalsIgnoreCase("rock")) {
            return 0;
        }
        if (string.equalsIgnoreCase("paper")) {
            return 1;
        }
        else {
            return 2;
        }
    }
}
