public enum HandType {
    PAPER, ROCK, SCISSORS;

    public boolean beats(HandType hand){
        switch (this){
            case ROCK:
                return hand = SCISSORS;
            case PAPER:
                return hand = ROCK;
            case SCISSORS:
                return hand = PAPER;
    }
    return false;
}
}
// my version
// public enum HandType {PAPER, ROCK,SCISSORS;
//private static final HandType[] VALUES = values();
//    private static final int SIZE = VALUES.length;
//    private static final Random RANDOM = new Random();}
