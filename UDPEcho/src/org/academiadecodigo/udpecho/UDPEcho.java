package org.academiadecodigo.udpecho;

import java.io.IOException;
import java.net.*;

public class UDPEcho {

    public static void main(String[] args) {

        DatagramSocket socket = null;

        if (args.length < 2) {
            throw new IllegalArgumentException("Usage:<port>");
        }

        int port = Integer.parseInt(args[1]);

            try {
                byte[] sendBuffer;
                byte[] recvBuffer = new byte[1025];
                socket = new DatagramSocket(port);

                while(true) {
                    DatagramPacket receivePacket = new DatagramPacket(recvBuffer, recvBuffer.length);
                    socket.receive(receivePacket);

                    String receivedPacket = new String(recvBuffer, receivePacket.getOffset(), receivePacket.getLength()).toUpperCase();
                    sendBuffer = receivedPacket.getBytes();
                    System.out.println(receivedPacket);

                    receivePacket.setData(sendBuffer);
                    socket.send(receivePacket);
                }

            } catch (SocketException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                if (socket != null) {
                    socket.close();
                }
            }
        }
}
