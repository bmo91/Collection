package org.academiadecodigo.bootcamp;

import org.academiadecodigo.bootcamp.scanners.integer.IntegerInputScanner;
import org.academiadecodigo.bootcamp.scanners.menu.MenuInputScanner;
import org.academiadecodigo.bootcamp.scanners.string.PasswordInputScanner;
import org.academiadecodigo.bootcamp.scanners.string.StringInputScanner;

public class Main {
    public static void main(String[] args) {

        String[] options = {"Playing", "Quit"};

        Prompt prompt = new Prompt(System.in, System.out);

        MenuInputScanner menu = new MenuInputScanner(options);
        menu.setMessage("Choose an option: ");
        int userInput = prompt.getUserInput(menu);


        StringInputScanner ask1 = new StringInputScanner();
        ask1.setMessage("Set Username");


        PasswordInputScanner pass = new PasswordInputScanner();
        pass.setMessage("Set password");


        IntegerInputScanner ask3 = new IntegerInputScanner();
        ask3.setMessage("Set age");



        String name = prompt.getUserInput(ask1);
        String password = prompt.getUserInput(pass);
        int age = prompt.getUserInput(ask3);

        System.out.println("Username: " + name + " | Password: ***" + " | User's age " + age);


    }
}

