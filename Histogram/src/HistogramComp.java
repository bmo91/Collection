import java.util.*;

public class HistogramComp implements Iterable<String>{

    private Map<String, Integer> map;
    public HistogramComp(String string){
        map = new HashMap<>();
    }

    public void Split(String string) {
        toString().split(string);
    }

    public void addWord(String word, Integer value) {
        if(map.containsKey(word)){
            map.put(word, value++);
        } map.put(word, 1);
    }

    public int size(){
        return map.size();
    }

    public int get(String word){
        return map.get(word);
    }

    @Override
    public Iterator<String> iterator(){
        return map.keySet().iterator();
    }
}

/*
private Map<String, Integer> map;

public HistogramComp(String text){
map = new HasMap<>();

for(String word : text.split(""){
map.put(word, map.containsKey(word) ? map.containsValue();}
 */