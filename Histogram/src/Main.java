
public class Main {

    public static final String STRING = "test word words test 1 10 1";

    public static void main(String[] args) {

        HistogramComp wordHistogram = new HistogramComp(STRING);
        System.out.println("MAP HAS " + wordHistogram.size() + " distinc words");

        for (String word : wordHistogram) {
            System.out.println(word + " : " + wordHistogram.get(word));
        }
    }
}
